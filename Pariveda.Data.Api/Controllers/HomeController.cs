﻿using System.Web.Mvc;

namespace Pariveda.Data.Api.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return new RedirectResult("/docs/index");
        }
    }
}
