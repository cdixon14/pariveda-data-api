﻿using Pariveda.Data.Api.Database;
using Pariveda.Data.Api.Models;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web.Http;

namespace Pariveda.Data.Api.Controllers
{
    [Authorize]
    public class PricingWorksheetController : ApiController
    {
        private SqlConnectionStringBuilder _builder;
        private SqlConnection _connection;

        [HttpGet]
        [ActionName("costs")]
        public IEnumerable<CostLine> GetCohortCosts()
        {
            // Open connection to Pricing Worksheet DB
            _builder = new SqlConnectionStringBuilder
            {
                DataSource = Config.PWHostname,
                UserID = Config.PWUserId,
                Password = Config.PWPassword,
                InitialCatalog = Config.PWDatabaseName,
            };

            var costs = new List<CostLine>();

            try
            {
                using (_connection = new SqlConnection(_builder.ConnectionString))
                {
                    var sb = new StringBuilder();
                    sb.Append(
                        "SELECT [Level], [Office], [Cost] " +
                        "FROM [dbo].[CostRates]");

                    var sql = sb.ToString();

                    using (var command = new SqlCommand(sql, _connection))
                    {
                        _connection.Open();
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                costs.Add(new CostLine
                                {
                                    Level = reader.GetString(0),
                                    Office = reader.GetString(1),
                                    Cost = reader.GetDouble(2),
                                });
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                throw e;
            }

            _connection.Close();
            return costs;
        }
    }
}