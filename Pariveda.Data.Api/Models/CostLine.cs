﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pariveda.Data.Api.Models
{
    public class CostLine
    {
        public string Level { get; set; }
        public string Office { get; set; }
        public double Cost { get; set; }
    }
}